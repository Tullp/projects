
import os
from typing import Type

from passlib.context import CryptContext

def ask[T](message: str, default: T = None, value_type: Type[T] = str, required: bool = False):
	if default is not None:
		message += f" (default: {default})"
	value = input(f"{message}: ").strip()
	if required and not value:
		raise Exception("Required field")
	if not value:
		return default
	return value_type(value)

config = {
	"PP_SERVICES_PATH": ask("Enter path to the services folder", "/etc/systemd/system/"),
	"PP_PROJECTS_PATH": ask("Enter path to the projects folder", required=True),
	"PP_BOT_TOKEN": ask("Enter your bot token"),
	"PP_OWNER_TELEGRAM_ID": ask("Enter your Telegram id", value_type=int, default=0),
	"PP_API_LOCAL_PORT": ask("Enter local port", value_type=int, default=0),
	"PP_API_SHARED_PORT": ask("Enter shared port", default=443, value_type=int),
	"PP_DOMAIN_NAME": ask("Enter server domain name"),
	"PP_CERT_PATH": ask("Enter path to SSL certificate", default="/root/ssl/cert.pem"),
	"PP_KEY_PATH": ask("Enter path to SSL certificate key", default="/root/ssl/key.pem"),
	"PP_API_PASSWORD": ask("Enter a password to access PetProject's API", "12345",
		value_type=CryptContext(schemes=["pbkdf2_sha256"], default="pbkdf2_sha256").hash),
}

with open(os.path.join(os.path.dirname(__file__), ".env"), "w") as f:
	f.write("\n".join(f"{k}={v}" for k, v in config.items()))
