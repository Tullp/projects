
import React, {useContext} from 'react';
import styles from './sidebar.module.scss';
import {AppContext} from './appcontext';
import {cn} from './utils';

export const Sidebar = () => {
  const {setService, services} = useContext(AppContext);
  const changeService = (service: string) => {
    window.scrollTo(0, 0);
    setService(service);
  }
  return <div className={styles.sidebar}>
    <div className={styles.projects}>
      {Object.keys(services).map((s) => {
        const is_active = services[s].state === 'active';
        return <div className={styles.project} onClick={() => changeService(s)} key={s}>
          <div className={styles.status} style={{backgroundColor: is_active ? '#4caf50' : '#f44336'}}/>
          <div>{s}</div>
          <i className={cn("fas fa-trash", styles.deleteIcon)}/>
        </div>;
      })}
    </div>
    <button className={styles.addProject}>+ Add Project</button>
  </div>;
};
