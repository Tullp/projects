import {is_dev} from './utils';

const API_TOKEN_KEY = 'PP_API_TOKEN';

const sendRequest: (endpoint: string, body?: any) => Promise<any> = async (endpoint: string, body: any = {}) => {
  body.token = localStorage.getItem(API_TOKEN_KEY) || '';
  const response = await fetch(`/api/${endpoint}`, {
    method: 'POST',
    headers: {'content-type': 'application/json'},
    body: JSON.stringify(body)
  });
  if (response.status === 401) {
    body.token = await authenticate();
    return await sendRequest(endpoint, body);
  }
  return await response.json();
}

const authenticate: (first?: boolean) => Promise<string> = async (first: boolean = true) => {
  if (is_dev())
    return 'test_token';
  const password = prompt((first ? '' : 'Incorrect password\n') + 'Enter API password') || '';
  const response = await fetch(`/api/auth`, {
    method: 'POST',
    headers: {'content-type': 'application/json'},
    body: JSON.stringify({password})
  });
  if (response.status === 401)
    return await authenticate(false);
  const auth = await response.json();
  localStorage.setItem(API_TOKEN_KEY, auth.token);
  return auth.token;
}

export interface iServices {
  [service: string]: {
    state: 'active' | 'inactive' | 'failed';
    start_ts: number;
  }
}

export const getServices: () => Promise<iServices> = () => {
  if (is_dev()) {
    return new Promise((res) => res({
      'Project 1': {state: 'active', start_ts: 0},
      'Project 2': {state: 'active', start_ts: 0},
      'Project 3': {state: 'failed', start_ts: 0},
      'Project 4': {state: 'inactive', start_ts: 0},
      'Project 5': {state: 'active', start_ts: 0},
    }));
  }
  return sendRequest('services')
    .then((response) => response.services);
};

export const restartService: (service: string) => void = (service: string) => {
  if (!is_dev()) void sendRequest(`restart/${service}`);
};

export const startService: (service: string) => void = (service: string) => {
  if (!is_dev()) void sendRequest(`start/${service}`);
};

export const stopService: (service: string) => void = (service: string) => {
  if (!is_dev()) void sendRequest(`stop/${service}`);
};

export const getLogs: (service: string) => Promise<string> = (service: string) => {
  if (is_dev())
    return new Promise((res) => res('[2023-05-25 14:32:45] INFO: Project 1 started\n[2023-05-25 14:32:50] ERROR: Project 2 failed to start\n[2023-05-25 14:33:00] INFO: Checking for updates...'));
  return sendRequest(`logs/${service}`)
    .then((response) => response.logs);
};
