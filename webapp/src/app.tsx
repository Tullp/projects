import React from 'react';
import {AppContextProvider} from './appcontext';
import {Header} from './header';
import styles from './app.module.scss';
import {Sidebar} from './sidebar';
import {Panel} from './panel';

export const App = () => {
  return <AppContextProvider>
    <Header/>
    <div className={styles.container}>
      <Sidebar/>
      <Panel/>
    </div>
  </AppContextProvider>;
}
