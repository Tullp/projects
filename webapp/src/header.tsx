
import React from 'react';
import styles from './header.module.scss';

export const Header = () => {
  return <header className={styles.header}>
    <div className={styles.logo}>PetProjects Dashboard</div>
    <nav className={styles.nav}>
      <a href="#">Home</a>
      <a href="#">Settings</a>
      <a href="#">Profile</a>
    </nav>
  </header>;
};
