import React, {Fragment, useContext, useEffect, useState} from 'react';
import styles from './panel.module.scss';
import {AppContext} from './appcontext';
import {getLogs, restartService, startService, stopService} from './api';
import {getLogger} from './utils';

const capitalize = (word: string) => {
  if (!word) return word;
  return word[0].toUpperCase() + word.slice(1).toLowerCase();
};

export const Panel = () => {
  const {services, service, isActive} = useContext(AppContext);
  const [logs, setLogs] = useState<string>('');
  useEffect(() => {
    if (!service)
      return;
    const log = getLogger('logs-updater');
    const action = () => {
      if (isActive()) {
        getLogs(service).then((l) => setLogs(l));
        log('logs updated');
      } else log('skip logs update');
    }
    action();
    const interval = setInterval(action, 5000);
    return () => clearInterval(interval);
  }, [service]);
  return <div className={styles.panel}>
    {service && <Fragment>
      <div className={styles.toolbar}>
        <button>Check for Updates</button>
        <button>Create Service</button>
      </div>
      <div className={styles.projectCard}>
        <h2>{service}</h2>
        <p>Status: {capitalize(services[service].state)}</p>
        <div className={styles.actions}>
          <button className={styles.start} onClick={() => startService(service)}>Start</button>
          <button className={styles.stop} onClick={() => stopService(service)}>Stop</button>
          <button className={styles.restart} onClick={() => restartService(service)}>Restart</button>
        </div>
      </div>
      <div className={styles.logsArea}>
        <h3>Logs</h3>
        <pre>{logs}</pre>
      </div>
    </Fragment>}
  </div>;
};
