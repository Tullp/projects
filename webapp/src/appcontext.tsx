
import React, {createContext, useEffect, useMemo, useState} from 'react';
import {getServices, iServices} from './api';
import {getLogger, useActivityMonitor} from './utils';

interface iAppContext {
  services: iServices;
  service: string;
  setService: (s: string) => void;
  isActive: () => boolean;
}

export const AppContext = createContext({} as iAppContext);

export const AppContextProvider = ({children}: { children: React.ReactNode }) => {
  const qs = useMemo(() => new URLSearchParams(window.location.search), []);
  const [services, setServices] = useState<iServices>({});
  const [service, setService] = useState<string>('');
  const {isActive} = useActivityMonitor();
  const state = useMemo<iAppContext>(() => {
    return {services, service, setService, isActive};
  }, [services, service, isActive]);
  useEffect(() => {
    const log = getLogger('services-updater');
    const action = () => {
      if (isActive()) {
        getServices().then((services) => setServices(services));
        log('services updated');
      } else log('skip services update');
    };
    action();
    const onFocus = () => action();
    window.addEventListener('focus', onFocus);
    const interval = setInterval(action, parseInt(qs.get('interval') || '60000'));
    return () => {
      clearInterval(interval);
      window.removeEventListener('focus', onFocus);
    };
  }, [qs]);
  return <AppContext.Provider value={state}>
    {children}
  </AppContext.Provider>
};
