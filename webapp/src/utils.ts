import {useCallback, useEffect, useRef} from 'react';

export const cn = (...classes: string[]) => {
  return classes.filter((c) => !!c).join(' ');
}

export const is_dev = () => process.env.NODE_ENV === "development";

export const getLogger = (source: string) => {
  return (message: string) => console.log(`${new Date().toLocaleTimeString()} - ${source} - ${message}`);
};

export const useActivityMonitor = () => {
  const log = getLogger('activity-monitor');
  const isUserLeave = useRef<boolean>(false);
  const lastMove = useRef<number>(Date.now());
  const onBlur = () => {
    isUserLeave.current = true;
    log('user leaved');
  };
  const onFocus = () => {
    isUserLeave.current = false;
    log('user returned');
  };
  const onMouseMove = () => {
    lastMove.current = Date.now();
  };
  useEffect(() => {
    window.addEventListener('blur', onBlur);
    window.addEventListener('focus', onFocus);
    window.addEventListener('mousemove', onMouseMove);
    return () => {
      window.removeEventListener('blur', onBlur);
      window.removeEventListener('focus', onFocus);
    }
  }, []);
  const isActive = useCallback(() => {
    return !isUserLeave.current && Date.now() - lastMove.current < 2 * 60 * 1000;
  }, [isUserLeave, lastMove]);
  return {isActive};
};
