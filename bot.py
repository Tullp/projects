
import os
import sys
import traceback
from typing import Callable
from http.client import RemoteDisconnected
from requests.exceptions import ConnectionError, ReadTimeout
from urllib3.exceptions import ProtocolError

from telebot import TeleBot, ExceptionHandler
from telebot.util import smart_split
from telebot.types import Message

from logs import Logger
from config import bot_check, home, bot_token, me
from cli import commander

bot_check()

class TelegramExceptionHandler(ExceptionHandler):

	def handle(self, exception):
		if isinstance(exception, (RemoteDisconnected, ProtocolError, ConnectionError, ReadTimeout)):
			return True
		for part in smart_split(traceback.format_exc()):
			bot.send_message(me, part)
		return True

class StdoutMock:

	def __init__(self, listener: Callable[[str], None]):
		self.__stdout = sys.stdout
		self.__listener = listener

	def write(self, message: str):
		if message.strip():
			self.__listener("\n" + message)

	def restore(self):
		sys.stdout = self.__stdout

bot = TeleBot(bot_token, exception_handler=TelegramExceptionHandler())
logger = Logger("bot")

@bot.message_handler(func=lambda msg: msg.chat.id == me)
def commands_handler(message: Message):
	os.chdir(home)
	def listener(text: str):
		for part in smart_split(text):
			bot.send_message(message.chat.id, part, reply_to_message_id=message.message_id)
	Logger.subscribe(listener)
	sys.stdout = StdoutMock(listener)
	commander.process(message.text.split(" ", 1 if message.text.startswith("exec ") else -1))
	Logger.unsubscribe()
	sys.stdout.restore()

if __name__ == "__main__":
	bot.infinity_polling()
