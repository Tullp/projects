
import os
import time
from datetime import datetime

from petcmd import Commander

from logs import Logger
from config import services_path, home, root
from tools.git import clone_repository, get_remote_state, get_local_state, pull
from tools.project import create_venv, install_requirements
from tools.services import restart_service, create_project_service, create_updates_service, stop_service, \
	get_service_status, get_services, delete_service, create_bot_service, create_api_service, get_service_state

os.chdir(home)
logger = Logger("cli")
commander = Commander()

@commander.command("init")
def init_project_command(project: str):
	"""
	Initialize a project

	:param project: project name, should match name from gitlab
	"""
	clone_repository(project)
	os.chdir(os.path.join(os.getcwd(), project))
	create_venv()
	install_requirements()
	create_project_service(project)
	if f"projects.updates.service" not in os.listdir(services_path):
		create_updates_service()
	os.chdir(home)

@commander.command("clone")
def clone_repository_command(repository: str):
	"""
	Clone a gitlab repository

	:param repository: repository name to clone
	"""
	clone_repository(repository)

@commander.command("venv")
def create_venv_command(project: str):
	"""
	Create a virtual environment for the given project

	:param project: project to create venv for
	"""
	if project not in os.listdir(home):
		print(f"Project {project} does not exist")
		return
	os.chdir(os.path.join(os.getcwd(), project))
	create_venv()

@commander.command("reqs")
def install_requirements_command(project: str):
	"""
	Install requirements.txt for the given project

	:param project: project to install requirements for
	"""
	if project not in os.listdir(home):
		print(f"Project {project} does not exist")
		return
	os.chdir(os.path.join(os.getcwd(), project))
	install_requirements()

@commander.command("create-service")
def create_service_command(project: str):
	"""
	Create a systemd service for the given project

	:param project: project to create service for
	"""
	if project not in os.listdir(home):
		print(f"Project {project} does not exist")
		return
	create_project_service(project)

@commander.command("pull")
def pull_changes_command(project: str):
	"""
	Pull git changes for the given project

	:param project: project to pull changes for
	"""
	if project not in os.listdir(home):
		print(f"Project {project} does not exist")
		return
	os.chdir(os.path.join(os.getcwd(), project))
	pull()

@commander.command("check-updates")
def check_updates_command(project: str = None, infinity: bool = False, interval: int = 60):
	"""
	Check updates for all projects

	:param project: project name to check for updates
	:param infinity: start an infinity updates checking cycle
	:param interval: interval between updates checks
	"""
	while True:
		with open(os.path.join(home, "lastprojectscheck.time"), "w") as file:
			file.write(str(datetime.now()))
		for service in get_services():
			if project is not None and project != service:
				continue
			logger.log(f"check {service}")
			os.chdir(os.path.join(home, service))
			if get_local_state() != get_remote_state():
				logger.log(f"pull {service}")
				pull()
				install_requirements()
				restart_service(service)
			os.chdir(home)
		if not infinity:
			break
		time.sleep(interval)

@commander.command("services")
def show_services_command():
	"""Show list of all services"""
	print("\n".join(get_services()))

@commander.command("stop")
def stop_service_command(name: str):
	"""
	Stop the given service

	:param name: service name to stop
	"""
	stop_service(name)

@commander.command("restart")
def restart_service_command(name: str):
	"""
	Restart the given service

	:param name: service name to restart
	"""
	restart_service(name)

@commander.command("status")
def show_service_status_command(name: str):
	"""
	Show the given service status

	:param name: service name to show status
	"""
	print(get_service_status(name))

@commander.command("state")
def show_service_status_command(name: str):
	"""
	Show the given service state

	:param name: service name to show state
	"""
	print(get_service_state(name))

@commander.command("delete")
def delete_service_command(name: str):
	"""
	Delete the given service

	:param name: service name to delete
	"""
	delete_service(name)

@commander.command("create-updates-service")
def create_updates_service_command():
	"""Remove if exists and create the updates service"""
	print(f"Name: projects.updates.service")
	create_updates_service()

@commander.command("create-bot-service")
def create_bot_service_command():
	"""Remove if exists and create the bot service"""
	print(f"Name: projects.bot.service")
	create_bot_service()

@commander.command("create-api-service")
def create_api_service_command():
	"""Remove if exists and create the api service"""
	print(f"Name: projects.api.service")
	create_api_service()

@commander.command("self-update")
def update_self_command(source: str = "cli"):
	"""
	Update the pet project and restart all inner services

	:param source: The source from which the command was called. Possible values: cli, api, bot.
	"""
	os.chdir(root)
	logger.log("updating self")
	pull()
	install_requirements()
	# the source service should be restarted at the last
	inner_services = ["updates", "api", "bot"]
	if source in inner_services:
		inner_services.remove(source)
		inner_services.append(source)
	for service in inner_services:
		restart_service(service)

@commander.command("webapp-update")
def update_webapp_command():
	"""Update the pet project's web dashboard and restart api service"""
	os.chdir(root)
	logger.log("updating webapp")
	pull()
	install_requirements()
	os.chdir(os.path.join(root, "webapp"))
	logger.log("building webapp")
	os.system("npm run build")
	restart_service("api")

@commander.command("recreate-services")
def recreate_services_command():
	"""Delete all existing services and recreate them"""
	for service in get_services():
		create_project_service(service)

@commander.command("exec")
def exec_cmd_command(command: str):
	"""
	Execute the given cmd expression

	:param command: a command to execute
	"""
	print(os.popen(command).read())

if __name__ == "__main__":
	commander.process()
