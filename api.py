
import uuid
from datetime import datetime, timedelta

import uvicorn
from fastapi import FastAPI, HTTPException, status
from fastapi.responses import FileResponse
from fastapi.staticfiles import StaticFiles
from passlib.context import CryptContext
from petdb import PetDB
from pydantic import BaseModel

from config import api_check, local_port, api_password
from logs import Logger
from tools.services import get_service_state, get_services, get_all_services, restart_service, start_service, \
	stop_service, get_service_logs, get_service_start_dt

api_check()

logger = Logger("api")
app = FastAPI()
app.mount("/static", StaticFiles(directory="webapp/build/static"))
db = PetDB.get()
tokens = db["tokens"]
pwd_context = CryptContext(schemes=["pbkdf2_sha256"], default="pbkdf2_sha256")

class CredentialsModel(BaseModel):
	password: str

class TokenModel(BaseModel):
	token: str

def check_auth(auth: TokenModel):
	if not tokens.exists({"token": auth.token, "expires": {"$gt": datetime.now().timestamp()}}):
		raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED, detail="Could not validate token")

@app.post("/api/auth")
def authenticate(credentials: CredentialsModel):
	if not pwd_context.verify(credentials.password, api_password):
		raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED, detail="Incorrect password")
	return tokens.insert({
		"token": str(uuid.uuid4()),
		"expires": (datetime.now() + timedelta(days=30)).timestamp()
	})

@app.post("/api/services")
def get_services_info(auth: TokenModel):
	check_auth(auth)
	return {"ok": True, "services": {
		service: {
			"state": get_service_state(service),
			"start_ts": get_service_start_dt(service).timestamp(),
		}
		for service in get_services() + ["updates", "bot", "api"]
	}}

@app.post("/api/restart/{service}")
def _restart_service(auth: TokenModel, service: str):
	check_auth(auth)
	if service not in get_all_services():
		return {"ok": False, "error": "Nonexistent service"}
	restart_service(service)
	return {"ok": True}

@app.post("/api/start/{service}")
def _start_service(auth: TokenModel, service: str):
	check_auth(auth)
	if service not in get_all_services():
		return {"ok": False, "error": "Nonexistent service"}
	start_service(service)
	return {"ok": True}

@app.post("/api/stop/{service}")
def _stop_service(auth: TokenModel, service: str):
	check_auth(auth)
	if service not in get_all_services():
		return {"ok": False, "error": "Nonexistent service"}
	stop_service(service)
	return {"ok": True}

@app.post("/api/logs/{service}")
def _get_service_logs(auth: TokenModel, service: str):
	check_auth(auth)
	if service not in get_all_services():
		return {"ok": False, "error": "Nonexistent service"}
	return {"logs": get_service_logs(service)}

@app.get("/")
def index():
	return FileResponse("webapp/build/index.html")

if __name__ == "__main__":
	uvicorn.run("api:app", host="127.0.0.1", port=local_port)
