
import os
import threading
from datetime import datetime
from typing import Callable

from config import root

class Logger:

	__listeners: dict[int, Callable[[str], None]] = {}

	def __init__(self, src: str):
		self.__src = src

	def log(self, message: str):
		with open(os.path.join(root, "logcat.txt"), "a") as f:
			f.write(self.__format(message))
		if threading.current_thread().ident in self.__listeners:
			self.__listeners[threading.current_thread().ident](self.__format(message))

	@classmethod
	def subscribe(cls, listener: Callable[[str], None]):
		cls.__listeners[threading.current_thread().ident] = listener

	@classmethod
	def unsubscribe(cls):
		cls.__listeners.pop(threading.current_thread().ident, None)

	def __format(self, message: str):
		return f"{self.__datetime():<26}: [{self.__src:^10}] {message}\n"

	@staticmethod
	def __datetime():
		return datetime.now().strftime("%Y-%m-%d %H:%M:%S.%f")
