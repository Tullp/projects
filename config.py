
import os

from dotenv import load_dotenv

load_dotenv()

services_path = os.environ.get("PP_SERVICES_PATH")

root = os.path.dirname(__file__)
home = os.environ.get("PP_PROJECTS_PATH")

bot_token = os.environ.get("PP_BOT_TOKEN")
me = int(os.environ.get("PP_OWNER_TELEGRAM_ID") or 0)

local_port = int(os.environ.get("PP_API_LOCAL_PORT") or 0)
shared_port = int(os.environ.get("PP_API_SHARED_PORT") or 0)
domain = os.environ.get("PP_DOMAIN_NAME")
cert_path = os.environ.get("PP_CERT_PATH")
key_path = os.environ.get("PP_KEY_PATH")
api_password = os.environ.get("PP_API_PASSWORD")

def bot_check():
	if not bot_token:
		raise Exception("Bot token is required")
	if not me:
		raise Exception("Owner's ID is required")

def api_check():
	if not local_port:
		raise Exception("Local API port is required")
	if not shared_port:
		raise Exception("Shared API port is required")
	if not domain:
		raise Exception("Domain name is required")
	if not cert_path:
		raise Exception("SSL certificate path is required")
	if not key_path:
		raise Exception("SSL certificate key path is required")
	if not api_password:
		raise Exception("API password is required")

if services_path is None:
	raise Exception("Path to services folder is required")
if home is None:
	raise Exception("Path to projects folder is required")
