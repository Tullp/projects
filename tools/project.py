
import os
import sys
from logs import Logger

logger = Logger("project")

def create_venv():
	if os.path.exists("venv"):
		return logger.log("Virtual environment already created")
	logger.log("Creating virtual environment...")
	os.system(f"{sys.executable} -m venv venv")

def install_requirements():
	if "requirements.txt" not in os.listdir():
		return logger.log("requirements.txt wasn't found")
	logger.log("Install requirements...")
	os.system(f"./venv/bin/python -m pip install -r requirements.txt")
