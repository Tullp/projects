
import os
from datetime import datetime

from config import services_path, root, home
from logs import Logger

logger = Logger("services")

service_template = """[Unit]
Description={description}

[Service]
User=root
Environment="LD_LIBRARY_PATH=/usr/local/lib"
Environment="PYTHONUNBUFFERED=1"
WorkingDirectory={directory}
ExecStart={cmd}
Restart=always

[Install]
WantedBy=multi-user.target"""

def service_config(description: str, directory: str, cmd: str):
	return service_template.format(description=description, directory=directory, cmd=cmd)

def create_service(name, content):
	path = os.path.join(services_path, f"projects.{name}.service")
	if os.path.exists(path):
		delete_service(name)
	logger.log(f"Creating service {name}...")
	with open(path, "w") as file:
		file.write(content)
	logger.log("Reloading systemctl...")
	os.system(f"sudo systemctl daemon-reload")
	logger.log("Enabling service...")
	os.system(f"sudo systemctl enable projects.{name}.service")
	logger.log("Starting service...")
	os.system(f"sudo systemctl start projects.{name}.service")

def get_service_status(name: str) -> str:
	return os.popen(f"sudo systemctl status projects.{name}.service").read().strip()

def get_service_state(name: str) -> str:
	return os.popen(f"sudo systemctl is-active projects.{name}.service").read().strip()

def get_service_start_dt(name: str) -> datetime:
	dt = os.popen(f"sudo systemctl show --property=ActiveEnterTimestamp projects.{name}.service | cut -d= -f2").read()
	return datetime.strptime(dt.strip(), "%a %Y-%m-%d %H:%M:%S UTC")

def get_service_logs(name: str) -> str:
	since = datetime.now().strftime("%Y-%m-%d 00:00:00")
	return os.popen(f"sudo journalctl -u projects.{name}.service --since '{since}' -r").read()

def restart_service(name: str):
	logger.log(f"Restarting service {name}...")
	os.system(f"sudo systemctl restart projects.{name}.service")

def start_service(name: str):
	logger.log(f"Restarting service {name}...")
	os.system(f"sudo systemctl start projects.{name}.service")

def stop_service(name: str):
	logger.log(f"Stopping service {name}...")
	os.system(f"sudo systemctl stop projects.{name}.service")

def disable_service(name: str):
	logger.log(f"Disabling service {name}...")
	os.system(f"sudo systemctl disable projects.{name}.service")

def delete_service(name: str):
	disable_service(name)
	stop_service(name)
	logger.log(f"Deleting service {name}...")
	os.system(f"rm /etc/systemd/system/projects.{name}.service")
	logger.log("Reloading systemctl...")
	os.system(f"sudo systemctl daemon-reload")

def create_project_service(project: str):
	logger.log("Creating project service...")
	create_service(project, service_config(
		description=f"Service for {project}",
		directory=os.path.join(home, project),
		cmd=f"{os.path.join(home, project, "venv", "bin", "python")} -u main.py"
	))

def create_updates_service():
	logger.log("Creating updates service...")
	create_service("updates", service_config(
		description="Updates service for Pet Projects",
		directory=home,
		cmd=f"{os.path.join(root, "venv", "bin", "python")} -u {os.path.join(root, "cli.py")} check-updates -i"
	))

def create_bot_service():
	logger.log("Creating bot service...")
	create_service("bot", service_config(
		description="Bot service for Pet Projects",
		directory=root,
		cmd=f"{os.path.join(root, "venv", "bin", "python")} -u {os.path.join(root, "bot.py")}"
	))

def create_api_service():
	logger.log("Creating api service...")
	create_service("api", service_config(
		description="API service for Pet Projects",
		directory=root,
		cmd=f"{os.path.join(root, "venv", "bin", "python")} -u {os.path.join(root, "api.py")}"
	))

def get_services():
	return [service for service in get_all_services() if service not in ("updates", "bot", "api")]

def get_all_services():
	return [service.split(".", 1)[-1].rsplit(".", 1)[0] for service in os.listdir(services_path)
		if service.startswith("projects.") and service.endswith(".service")]
