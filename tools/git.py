
import os

from logs import Logger

logger = Logger("git")

def clone_repository(project: str):
	if os.path.exists(project):
		return logger.log(f"Project {project} already cloned")
	logger.log("Cloning repository...")
	os.system(f"git clone https://gitlab.com/Tullp/{project}.git")

def get_local_state():
	return os.popen("git rev-parse HEAD").read().strip()

def get_remote_state():
	return os.popen("git ls-remote | grep HEAD").read().split("\t")[0].strip()

def pull():
	logger.log("Pulling repository...")
	os.system("git pull")
